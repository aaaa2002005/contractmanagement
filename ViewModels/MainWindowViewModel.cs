﻿using ContractManagement.Commands;
using ContractManagement.Models;
using ContractManagement.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ContractManagement.ViewModels
{
    /// <summary>
    /// 主界面ViewModel
    /// </summary>
    public class MainWindowViewModel : NotificationObject
    {
        private Contract contract;

        public DelegateCommand ImportContractCommand { get; private set; }

        public DelegateCommand PrintContractCommand { get; private set; }

        public DelegateCommand ViewContractListCommand { get; private set; } //查看合同列表命令，待实现

        /// <summary>
        /// 主界面ViewModel的构造函数，负责生成命令
        /// </summary>
        public MainWindowViewModel()
        {
            this.ImportContractCommand = new DelegateCommand
            {
                ExecuteAction = new Action<object>(ImportContract)
            };
            this.PrintContractCommand = new DelegateCommand
            {
                ExecuteAction = new Action<object>(PrintContract)
            };
        }
        
        /// <summary>
        /// 合同导入按钮Binding事件
        /// </summary>
        /// <param name="parameter">DelegateCommand传入的参数</param>
        private void ImportContract(object parameter)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                Filter = "Selected File|*.docx;*.doc;*.pdf",
                InitialDirectory = "C:\\",
                CheckFileExists = true,
                CheckPathExists = true
            };
            if (dialog.ShowDialog() == DialogResult.Cancel) //一般为用户关闭了OpenFileDialog
            {
                return;
            }
            
            try
            {
                if (dialog.FileName.Contains(".docx") || dialog.FileName.Contains(".doc"))
                {
                    contract = new ContractWord(dialog.FileName);
                }
                else //剩下的情况只可能是.pdf文件
                {
                    contract = new ContractPdf(dialog.FileName);
                }
                contract.ContractFilePath = dialog.FileName;
                ContractData data = contract.ReadContractData();
                MessageBox.Show("合同导入成功");
                //TODO:将得到的信息传上数据库，待做
            }
            catch(Exception e)
            {
                MessageBox.Show(e.StackTrace);
                MessageBox.Show(e.Message);
            }
            
        }

        /// <summary>
        /// 打印合同按钮的Binding事件
        /// </summary>
        /// <param name="parameter">DelegateCommand传入的参数</param>
        private void PrintContract(object parameter)
        {
            if(contract == null)
            {
                MessageBox.Show("还未导入合同，请先选择合同导入!");
                ImportContract(null);
                if(contract == null) //用户可能取消，导致下面逻辑错误，应当返回
                {
                    return;
                }
            }
            if (contract.GetType() == typeof(ContractWord)) 
            {
                //TODO:如果为word类应该把word转成pdf，如何解决？
            }
            else
            {
                var contractPdf = contract as ContractPdf;
                bool isSucceeded = contractPdf.AddWaterMark();
                if(isSucceeded)
                {
                    MessageBox.Show("成功在Output文件夹下生成拥有水印的合同文件！文件名称：" + contractPdf.OutputContractName);
                }
                else
                {
                    MessageBox.Show("已在Output文件夹下成功生成拥有水印的合同文件，无需重复生成！文件名称：" + contractPdf.OutputContractName);
                }
            }
        }

    }
}
