﻿using ContractManagement.Commands;
using ContractManagement.Models;
using Microsoft.Win32;
using System;
using iText.IO;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Extgstate;
using iText.Kernel.Pdf.Canvas;
using iText.IO.Image;
using System.IO;
using System.Windows;
using System.Text;
using System.Threading.Tasks;
using iText.Kernel.Font;
using iText.IO.Font;
using iText.Kernel.Pdf.Canvas.Parser;
using System.Collections.Generic;

namespace ContractManagement.Models
{
    /// <summary>
    /// 合同pdf类
    /// </summary>
    internal class ContractPdf : Contract
    {
        private readonly ImageData image;

        private PdfDocument pdfDocument;

        /// <summary>
        /// 根据文件路径，生成ContractPdf实例
        /// </summary>
        public ContractPdf(string fileName)
        {  
            image = ImageDataFactory.Create("../../Images/WaterMark.jpg");
            this.Open(fileName);
        }

        /// <summary>
        /// 释放Pdf资源，防止泄露
        /// </summary>
        ~ContractPdf()
        {
            if(this.IsOpened())
            {
                this.Close();
            }
        }

        /// <summary>
        /// 打开合同Pdf文件
        /// </summary>
        /// <param name="fileName">文件路径</param>
        protected override void Open(string fileName) 
        {
            if(fileName != ContractFilePath) //与上一次不同，才需要更换
            {
                string path = "../../Output/" + GenerateContractName() + ".pdf";
                pdfDocument = new PdfDocument(new PdfReader(fileName), new PdfWriter(path));
            }
        }

        /// <summary>
        /// 关闭合同Pdf文件
        /// </summary>
        protected override void Close()
        {
            if(!pdfDocument.IsClosed())
            {
                pdfDocument.Close();
            }
        }

        /// <summary>
        /// 合同Pdf文件是否打开
        /// </summary>
        /// <returns>Pdf文件如果开启返回true，否则返回false</returns>
        public override bool IsOpened()
        {
            return !pdfDocument.IsClosed();
        }

        /// <summary>
        /// 合同Pdf文件是否关闭
        /// </summary>
        /// <returns>Pdf文件如果关闭返回true，否则返回false</returns>
        public override bool IsClosed()
        {
            return !IsOpened();
        }

        /// <summary>
        /// 对合同pdf文件生成水印
        /// </summary>
        /// <return></return>
        public bool AddWaterMark()
        {
            if(IsClosed())
            {
                return false;
            }
            image.SetHeight(200);
            image.SetWidth(200);
            PdfExtGState tranState = new PdfExtGState();
            tranState.SetFillOpacity(0.5f);
            for(int i = 1; i <= pdfDocument.GetNumberOfPages(); i++)
            {
                PdfPage page = pdfDocument.GetPage(i);
                PdfCanvas canvas = new PdfCanvas(page);
                canvas.SaveState().SetExtGState(tranState);
                canvas.AddImage(this.image, 0, 0, false);
                canvas.RestoreState();
                canvas.Release();
            }
            pdfDocument.Close(); //PdfDocument需要关闭才能生成水印... 巨坑
            return true;
        }

        /// <summary>
        /// 读取合同pdf文件的信息
        /// </summary>
        /// <returns>传入数据库的信息</returns>
        public override ContractData ReadContractData()
        {
            if (IsClosed())
            {
                throw new NullReferenceException("Pdf文件未被打开或者为空！");
            }

            ContractData data = new ContractData();
            try
            {
                //利用iText的文本提取器提取文字
                string content = PdfTextExtractor.GetTextFromPage(this.pdfDocument.GetPage(1));
                var dataList = new List<string>();
                var paragraphCount = 0;
                var paragraph = "";
                foreach (var c in content)
                {
                    paragraph += c;
                    if (c == '\n')
                    {
                        dataList.Add(paragraph);
                        paragraph = "";
                        paragraphCount++;
                        if (paragraphCount == data.GetType().GetProperties().Length + 1) //收集到全部信息
                        {
                            break;
                        }
                    }
                }
                dataList.RemoveAt(0); //删除标题
                this.AssignContractDataProperties(ref data, dataList);
            }
            catch
            {
                throw;
            }
            return data;
        }
    }
}
