﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractManagement.Models
{
    /// <summary>
    /// 合同中的关键信息
    /// </summary>
    public sealed class ContractData
    {
        public string FirstPartyName { get; set; }

        public string SecondPartyName { get; set; }

        public string BrandName { get; set; }

        public string BrandRegistrationNumber { get; set; }

        public string NextBrandRenewalTime { get; set; }

        public string ContractSignTime { get; set; }

        public string ContractNumber { get; set; }

    }
}
