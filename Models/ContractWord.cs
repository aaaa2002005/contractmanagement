﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Word = Microsoft.Office.Interop.Word;

namespace ContractManagement.Models
{
    /// <summary>
    /// 合同word类
    /// </summary>
    internal class ContractWord : Contract
    {
        private Word.Application application;

        private Word.Document document;

        private bool isClosed;

        /// <summary>
        /// 生成空的ContractWord实例
        /// </summary>
        public ContractWord(string fileName)
        {
            application = new Word.Application();
            isClosed = false;
            this.Open(fileName);
        }

        /// <summary>
        /// 释放资源，防止资源泄露
        /// </summary>
        ~ContractWord()
        {
            if(this.IsOpened())
            {
                this.Close();
            }
        }

        /// <summary>
        /// 打开合同Word
        /// </summary>
        /// <param name="fileName">文件绝对路径</param>
        protected override void Open(string fileName)
        {
            object file = fileName as object;
            object readOnly = true;
            object visible = false;
            object missing = System.Reflection.Missing.Value;
            document = application.Documents.Open(ref file, ref missing, ref readOnly, ref missing, ref missing, ref missing, ref missing, ref missing,
                                                  ref missing, ref missing, ref missing, ref visible, ref missing, ref missing, ref missing, ref missing);
            if(document == null)
            {
                throw new NullReferenceException("无法打开word合同文档");
            }
        }

        /// <summary>
        /// 关闭Word文件资源
        /// </summary>
        protected override void Close()
        {
            if(document != null && !this.isClosed)
            {
                this.isClosed = true;
                document.Close();
            }
        }

        /// <summary>
        /// 合同Word文件是已经打开
        /// </summary>
        /// <returns>打开返回true，否则返回false</returns>
        public override bool IsOpened()
        {
            return !this.isClosed;
        }

        /// <summary>
        /// 合同Word文件是已经关闭
        /// </summary>
        /// <returns>关闭返回true，否则返回false</returns>
        public override bool IsClosed()
        {
            return this.isClosed;
        }

        /// <summary>
        /// 读取合同word的信息
        /// </summary>
        /// <returns>传入数据库的信息</returns>
        public override ContractData ReadContractData()
        {
            //这里的Count == 0可以优化
            if (this.document == null || this.document.Paragraphs.Count == 0)
            {
                throw new NullReferenceException("当前合同文件为空");
            }

            //Note:Word段落索引默认是从1开始的，因此从2开始跳过标题
            var data = new ContractData();
            var dataList = new List<string>();
            for(int i = 2; i < data.GetType().GetProperties().Length + 2; i++)
            {
                dataList.Add(document.Paragraphs[i].Range.Text);
            }
            this.AssignContractDataProperties(ref data, dataList);
            return data;
        }

    }
}
