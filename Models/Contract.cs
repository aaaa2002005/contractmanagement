﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ContractManagement.Models
{
    /// <summary>
    /// 合同基类
    /// </summary>
    public abstract class Contract
    {
        public string ContractFilePath { set; get; } //合同文件路径

        public string OutputContractName { get; private set; } //打出水印的pdf文件的名称，为当前时间

        /// <summary>
        /// 提取合同传入数据库的信息的接口
        /// </summary>
        /// <returns>传入数据库的信息</returns>
        public abstract ContractData ReadContractData();

        /// <summary>
        /// 开启合同文件流
        /// </summary>
        /// <param name="fileName">文件路径</param>
        protected abstract void Open(string fileName);

        /// <summary>
        /// 关闭合同文件流
        /// </summary>
        protected abstract void Close();

        /// <summary>
        /// 合同文件是已经打开 
        /// </summary>
        /// <returns>打开返回true，否则返回false</returns>
        public abstract bool IsOpened();

        /// <summary>
        /// 合同文件是已经关闭
        /// </summary>
        /// <returns>关闭返回true，否则返回false</returns>
        public abstract bool IsClosed();

        /// <summary>
        /// 提取合同中需要传入数据库的信息
        /// </summary>
        /// <param name="paragraph">传入的段落</param>
        /// <returns></returns>
        private string ExtractContractData(string paragraph)
        {
            for (int i = 0; i < paragraph.Length; i++)
            {
                if (paragraph[i] == ':' || paragraph[i] == '：')
                {
                    paragraph = paragraph.Remove(0, i + 1);
                    break;
                }
            }
            return paragraph;
        }

        /// <summary>
        /// 对传入的ContractData实例的属性根据传入的List逐一进行赋值
        /// </summary>
        /// <param name="data">需要被赋值的ContractData对象</param>
        /// <param name="list">赋值的List，应与ContractData属性顺序一致</param>
        /// <returns>个数不一致返回false，成功返回true</returns>
        protected virtual bool AssignContractDataProperties(ref ContractData data, IList list)
        {
            if(data.GetType().GetProperties().Length != list.Count)
            {
                return false;
            }
            PropertyInfo[] properties = data.GetType().GetProperties();
            for (int i = 0; i < properties.Length; i++)
            {
                properties[i].SetValue(data as object, ExtractContractData(list[i] as string), null);
            }
            return true;
        }

        /// <summary>
        /// 生成符合文件名字标准的水印合同名称
        /// </summary>
        /// <returns>符合文件名字标准的水印合同名称</returns>
        protected virtual string GenerateContractName()
        {
            string name = DateTime.Now.ToString().Replace(" ", "_").Replace("/", "_").Replace(":", "_");
            OutputContractName = name;
            return name;
        }

    }
}
